**Paheal image scraper:**

Download all images matching your query. \
Supports searching by tags, excluding tags and excluding previously downloaded images. \
Will pull images from rule34.paheal.net. \
No installation required, list of Python dependencies below:

Dependencies: \
BeautifulSoup 4 \
urllib \
requests \
Python 3.6 or newer

Install these dependencies using pip:\
In a console, run: pip install bs4 urllib requests

Usage: \
Download the file 'scraper.py'. \
Open a console and call the script using python: \
python scraper.py [options] 


*Example:* \
python scraper.py -s ./mydownloads/ -t Porkyman -ex Pikachu Ash_Ketchum \
Make sure the folder you specify after '-s' exists before running the script


Internal help message:

Paheal image scraper script. scraper.py\
Part of this script can be user edited using any text editor.

Usage:\
   scraper.py -s <path> -t {tags} [-ex {tags}] [-ef {paths}]

Options:\
   -s <path>   Path to download to.\
   -t {tags}   List of tags to search for. Whitespace separated. At least one.\
   -ex {tags}  List of tags to exclude when downloading. Whitespace separated.\
               Optional parameter.\
   -ef {paths} List of paths to search for images to exclude.\
               Paths are searched recursively.\
               Image names must be formatted with the Paheal image number as\
               the prefix, separated by a dash.\
               The file contents don't actually matter.\
               Optional parameter.\
   -test       Run the script with preset tags, to test functionality.\
   -h, -?, --help Print this help message.

Usage example: scraper.py -s ./dl -t go -ex golang