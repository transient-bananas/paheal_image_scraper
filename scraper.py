"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>
"""


from bs4 import BeautifulSoup as bs
import urllib.request
import html
import time
import random
import requests
import os
import sys

# --- USER EDITABLE FURTHER BELOW ---

# Image class definition
class Image():
   def __init__(self, exclude_tags: list, exlude_ids: list):
      self.exlude_tags = exclude_tags
      self.exlude_ids = exlude_ids

      # Image attributes:
      # The link to the paheal image page
      self.r34_link = ""
      # The paheal image id
      self.r34_nr = 0
      # Image server link
      self.image_link = ""
      # Space separated tag string
      self.tags = ""
      # Image file size in bytes
      self.size = 0
      # Image file size as stored on paheal (eg: "375.4K", "2.4M")
      self.size_str = ""
      # Image resolution as a number (Pixel count)
      # Eg: for 860x320 .resolution would be 860 * 320 = 262400
      self.resolution = 0
      # Link to the thumbnail image displayed in the search results
      self.thumblink = ""
      # Image file type (jpg, png, gif, ...)
      self.filetype = ""
      # Name of the script will save this file as 
      # Generated as: "<paheal nr>-<tags>.<filetype>"
      self.filename = ""
      #
      self.alt_links = []

   # Image class functions: 
   def parse_title(self, string_in: str):
      items = string_in.split("//")
      self.tags = items[0].strip()
      self.filetype = items[3].strip()
      res_str = items[1].strip()
      size_str = items[2].strip()
      self.size_str = size_str
      
      res0, res1 = res_str.split("x", 2)
      self.resolution = int(res0.strip()) * int(res1.strip())
      
      if "KB" in size_str:
         size_mult = 1024
         size_str = size_str.replace("KB", "")
      elif "MB" in size_str:
         size_mult = 1024**2
         size_str = size_str.replace("MB", "")
      else:
         size_mult = 1
      
      self.size = int(float(size_str.strip()) * size_mult)

   def generate_filename(self):
      tags = self.tags
      if len(tags) > 190:
         tag_list = tags.split()
         tags = ""
         for tag in tag_list:
            tags += tag + " "
            if len(tags) > 185:
               tags += "... "
               break
         
      self.filename = "{nr}-{tags}.{file}" \
            .format(tags=tags, nr=self.r34_nr, file=self.filetype)


# --- USER EDITABLE BELOW ---

# You probably should know some basic Python syntax for this

# Image filter function:
# You can add filter conditions.
# Return False to reject an image
# Return True to immediatly accept an image, forgoing other conditions
def image_filter(image: Image) -> bool:
   # Standard exlusions from command line options:
   # exclude image we already have
   if image.r34_nr in image.exlude_ids:
      return False

   if any((exclude in image.tags for exclude in image.exlude_tags)):
      return False
   # End standard exclusions ----

   # Reference the Image class above for attributes you can check for filters
   # All except for 'image.filename' are set when 'image_filter' is called

   # Add filters here
   # -->

   # Example: exclude low resolution images
   #if image.resolution < 200**2:
   #   return False

   # Example: exclude images with over 150 tags
   #if len(image.tags) > 150:
   #   return False

   # <-- 
   # End image filters
   return True

def main(argv: list = None):

   # Add tags here to always exclude them:
   # Example: exclude = ["Ruile_63 Sonic_the_Hedgehog "Shitting_Dick_Nipples"]
   
   exclude = [] # <-- tags go in here ["<tag>", "<tag>", ...]

   # ---  END USER EDITABLE ---
   # If you want to modify / hack the code, go right ahead!
   # (make a copy first)

   if argv is None:
      args = sys.argv
   else:
      args = argv

   exclude.extend([])
   tags = []
   # Same folder as script is standard download folder
   save_to = "./" + args[0].rsplit("/", maxsplit=2)[0] + "/"
   exclude_folders = []

   option = ""
   help_options = ["-h", "-?", "--h", "--help"]
   options = ["-s", "-t", "-ex", "-ef", "-test"]
   help_message = \
   """
Paheal image scraper script. scraper.py
Part of this script can be user edited using any text editor.

Usage:
   scraper.py -s <path> -t {tags} [-ex {tags}] [-ef {paths}]

Options:
   -s <path>   Path to download to.
   -t {tags}   List of tags to search for. Whitespace separated. At least one.
   -ex {tags}  List of tags to exclude when downloading. Whitespace separated.
               Optional parameter.
   -ef {paths} List of paths to search for images to exclude.
               Paths are searched recursively.
               Image names must be formatted with the Paheal image number as
               the prefix, separated by a dash. 
               The file contents don't actually matter.
               Optional parameter.
   -test       Run the script with preset tags, to test functionality.
   -h, -?, --help Print this help message.

Usage example: scraper.py -s ./dl -t go -ex golang
"""

   if len(args) < 2:
      print(help_message)

   for arg in args[1:]:
      if "-" in arg:
         if arg in help_options:
            print(help_message)
            sys.exit()
         elif arg == "-test":  # Test option
            save_to = "./" + args[0].rsplit("/", maxsplit=2)[0] + "/"
            tags = ["go", "gopher"]
            exclude = []
            exclude_folders = []
            break
         elif arg in options:
            option = arg
         else:
            sys.exit("Unkown option '{}'!".format(arg))
      else:        
         if option == "-s":
            save_to = arg
         elif option == "-t":
            tags.append(arg)
         elif option == "-ex":
            exclude.append(arg)
         elif option == "-ef":
            exclude_folders.append(arg)
         else:
            print(help_message)
            sys.exit("No option set! Stopping...")

   count = get_images(tags, exclude, save_to, exclude_folders)
   print("Done")


hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

def get_url_soup(url: str):
   req = urllib.request.Request(url=url, headers=hdr)
   try:
      with urllib.request.urlopen(req) as response:
         html = response.read()
   except Exception as exc:
      print("URL Request failed: {}".format(exc))
      return None
   return bs(html, features="html.parser")

def download_from_paheal_search(url: str, excluded_tags: list,
                                saveto: str, exclude_ids: list,
                                page_count: int) -> int:
   images = []
   ex_id = get_exclude_nums(saveto)
   ex_id.extend(exclude_ids)

   soup = get_url_soup(url)
   if not soup:
      return (0, 0)
   ret = soup.find_all("a", attrs={"class": "shm-thumb-link"})
   for thumb in ret:
      image = Image(excluded_tags, ex_id)
      image.r34_link = thumb.get("href")
      image.r34_nr = int(image.r34_link.rsplit("/", maxsplit=2)[-1])
      image.parse_title(thumb.next.get("title"))
      image.thumblink = thumb.next.get("src")
      images.append(image)
   image_count = len(images)

   soup.decompose()

   print("got {} images for request {}".format(image_count, url))
   print("filtering...")

   # Filter images
   filtered_images = list(filter(image_filter, images))

   print("filtered images: {}".format(len(filtered_images)))

   prefix = "http://rule34.paheal.net"
   counter = 0
   # Grab images
   for image in filtered_images:
      url = prefix + image.r34_link
      print("image {} of page {}: {}".format(counter, page_count, url))
      counter += 1
      # Sleep briefly
      # We do this to not overload the paheal servers and
      # to mask our activity, appear more like regular traffic.
      # If we were to pull images as quickly as our connection would allow,
      # we would be like a clear spike of traffic from a single client in
      # the paheal server logs -> easily identifiable and blockable
      # Instead we throttle the download speed by waiting between actions
      # briefly. This way it appears as a quickly browsing client, going 
      # through a results list from a search one by one, plausible behaviour.
      # If this is too slow for you, consider running multiple searches in 
      # parallel, speeding the process up this way.
      # You could also code in a feature to automatically launch the script in 
      # parallel, scraping multiple different pages of the same search, 
      # something, I was too lazy for, so here's at least the idea :P  
      time.sleep(random.randint(1, 4) / 10)

      # load html site with image
      soup = get_url_soup(url)
      if not soup:
         print("Coudln't open {}".format(url))
         continue
      # extract image link
      img_sect = soup.find("section", {"id": "Imagemain"})
      if not img_sect:
         img_sect = soup.find("section", {"id": "Videomain"})
         img_block = img_sect.find("video", {"id": "main_image",
                                             "class": "shm-main-image"})
         img_block = img_block.find("source")
      else:
         img_block = img_sect.find("img", {"alt": "main image",
                                           "class": "shm-main-image"})
      image.image_link = img_block.get("src")

      # Extract the Image Info table
      links = soup.find("table", {"class": "image_info form"})
      
      for item in links.contents:
         item_text = str(item)
         # Search for the links section
         if "Links" in item_text:
            # The links are capsuled in quotation marks in HTML
            image.alt_links = [string for string in item_text.split('"')
                               if "paheal.net" in string]
      
      soup.decompose()
      
      print("getting {}".format(image.image_link))

      # Another sleep, right after we grabbed and parsed the image page
      # and before we download the image
      time.sleep(random.randint(1, 4) / 10)
      image.generate_filename()

      filepath = saveto + image.filename
      filepath = filepath.replace('?', '#')
      filepath = filepath.replace('"', "''")
      filepath = filepath.replace(':', "#")

      # Download image
      try:
         ret = urllib.request.urlretrieve(image.image_link,
                                          filename=filepath)
      except KeyboardInterrupt:
         try:
            os.remove(filepath)
         except FileNotFoundError:
            pass
         raise KeyboardInterrupt(counter)
      except:
         # Retry with alternate links
         for link in image.alt_links:
            print("Download failed! Retrying with alternate link: '{}'"
                  .format(link))
            try:
               ret = urllib.request.urlretrieve(link, filename=filepath)
            except KeyboardInterrupt:
               try:
                  os.remove(filepath)
               except FileNotFoundError:
                  pass
               raise KeyboardInterrupt(counter)
            except:
               pass
      if not ret:
         print("Download failed! Skipping image '{}'.".format(image.filename))
      else:
         print("wrote {}: {}".format(image.size_str, ret[0]))

   return (image_count, len(filtered_images))

def get_exclude_nums(folder: str) -> list:
   ex_id = []
   for filename in os.listdir(folder):
      num = filename.split("-")[0]
      if num.isdigit():
         ex_id.append(int(num))
         continue
      path = "{}/{}".format(folder, filename)
      # Recursively descend
      if os.path.isdir(path):
         rec_ex_id = get_exclude_nums(path)
         if rec_ex_id:
            ex_id.extend(rec_ex_id)
   return ex_id
   
def build_paheal_list_url(tags: list, count: int):

   if not tags or count < 1:
      return ""
   prefix = "http://rule34.paheal.net/post/list/"
   tag_string = ""
   tagsep = "%20"
   for tag in tags:
      tag_string = tag_string + tag + tagsep
   # remove excess tagsep string
   tag_string = tag_string[:len(tag_string) - len(tagsep)]
   return F"{prefix}{tag_string}/{count}"

def get_images(tags: list, excluded_tags: list, saveto: str,
               exclude_folders: list):
   if not tags:
      print("Nothing downloaded, tags empty!")
      return 0
   image_counter = 0
   list_counter = 1

   ex_ids = []

   # Gather ids of excluded images
   for folder in exclude_folders:
      ids = get_exclude_nums(folder)
      if ids:
         ex_ids.extend(ids)

   while True:
      t_before = time.time()
      print("Getting page {} for {}...".format(list_counter, tags))
      url = build_paheal_list_url(tags, list_counter)
      print(url)
      try:
         imgs_this_loop, downloaded = \
               download_from_paheal_search(url, excluded_tags, saveto,
                                          ex_ids, list_counter)
      except KeyboardInterrupt as keyint:
         image_counter += keyint.args[0]
         print("Loaded {} images total for query {}.".format(image_counter,
                                                             tags))
         return image_counter
      
      t_after = time.time()
      print("Loaded {} images in {} seconds".format(downloaded,
                                                    t_after - t_before))
      image_counter += downloaded
      if imgs_this_loop < 69:
         print("Loaded {} images total for query {}.".format(image_counter, tags))
         return image_counter
      
      list_counter += 1
      try:
         time.sleep(random.randint(1, 4) / 10)
      except KeyboardInterrupt:   
         t_after = time.time()
         print("Loaded {} images in {} seconds".format(downloaded,
                                                      t_after - t_before))
         return image_counter


if __name__ == "__main__":
   argv = None
   #argv = ("./image_scraper/scraper.py", 
    #       "-t", "Gumball_Watterson",
    #       "-ef", "image_scraper/exclude",
    #       "-s", "image_scraper/dl/")
   main(argv)
